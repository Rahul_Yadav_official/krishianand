package com.e.krishianand.api;

import com.e.krishianand.util.AppConstant;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitApi {

    private static final String BASE_URL = AppConstant.API_URL;


    private static apiRest mPostServices = null;

    public static apiRest getTickerAppObject() {
        if (null == mPostServices) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            mPostServices = retrofit.create(apiRest.class);
        }
        return mPostServices;
    }
}
