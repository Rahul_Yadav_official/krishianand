package com.e.krishianand.api;



import com.e.krishianand.ui.model.response.AddCCEFormModel;
import com.e.krishianand.ui.model.response.BlockModel;
import com.e.krishianand.ui.model.response.ChangePasswordResponse;
import com.e.krishianand.ui.model.response.CropNameModel;
import com.e.krishianand.ui.model.response.CropSeasonModel;
import com.e.krishianand.ui.model.response.CropSeasonPayload;
import com.e.krishianand.ui.model.response.DistrictModel;
import com.e.krishianand.ui.model.response.EditProfileResponse;
import com.e.krishianand.ui.model.response.LoginResponse;
import com.e.krishianand.ui.model.response.PanchayatModel;
import com.e.krishianand.ui.model.response.SignUpResponse;
import com.e.krishianand.ui.model.response.TehsilModel;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Tamim on 28/09/2017.
 */

public interface apiRest {
    @POST("apiv4/v3/RegisterUser")
    @FormUrlEncoded
    Call<SignUpResponse> signupApi(@FieldMap Map<String, String> params);

    @POST("apiv4/v3/LoginUser")
    @FormUrlEncoded
    Call<LoginResponse> LoginApi(@FieldMap Map<String, String> params);


    @POST("apiv4/v3/Updateprofile")
    @FormUrlEncoded
    Call<EditProfileResponse> EditProfileApi(@FieldMap Map<String, String> params);

    @POST("apiv4/v3/Updatepassword")
    @FormUrlEncoded
    Call<ChangePasswordResponse> changePAsswordApi(@FieldMap Map<String, String> params);


    @POST("apiv4/v3/Getcropseason")
    Call<CropSeasonModel> CropSeasonApi();

    @POST("apiv4/v3/Getcropname")
    Call<CropNameModel> CropNameApi();

    @POST("apiv4/v3/Getalldistrict")
    @FormUrlEncoded
    Call<DistrictModel> DistrictApi(@FieldMap Map<String, String> params);

    @POST("apiv4/v3/Getalltehsil")
    @FormUrlEncoded
    Call<TehsilModel> TehsilApi(@FieldMap Map<String, String> params);

    @POST("apiv4/v3/Getallgirdawar")
    @FormUrlEncoded
    Call<BlockModel> BlockApi(@FieldMap Map<String, String> params);

    @POST("apiv4/v3/Getpatwarhalka")
    @FormUrlEncoded
    Call<PanchayatModel> PanchayatApi(@FieldMap Map<String, String> params);

    @POST("apiv4/v3/RegisterUser")
    @FormUrlEncoded
    Call<AddCCEFormModel> addCceFormApi(@FieldMap Map<String, String> params);

   /* @POST("apiv4/v3/Addpayment")
    @FormUrlEncoded
    Call<PaymentAddResponse> addPaymentInfo(@FieldMap Map<String, String> params);

    @POST("apiv4/v3/Checksubcription")
    @FormUrlEncoded
    Call<PaymentInfo> getPaymentInfo(@FieldMap Map<String, String> params);*/
}
