package com.e.krishianand.api;

import android.content.Context;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.otto.Bus;

import java.util.ArrayList;

public class KrishiApplication extends MultiDexApplication {

    /**
     * Maintains a reference to the application context so that it can be
     * referred anywhere wihout fear of leaking. It's a hack, but it works.
     */
    private static Context sStaticContext;
    public ArrayList<String> myGlobalArray = null;
   // private PEApi sreApi;
    private static KrishiApplication applicationControler;
    /**
     * Reference to the bus (OTTO By Square)
     */
    private Bus mBus;

    /**
     * Gets a reference to the application context
     */

    public static synchronized KrishiApplication getInstance() {
        return applicationControler;
    }
    public static Context getStaticContext() {
        if (sStaticContext != null) {
            return sStaticContext;
        }

        //Should NEVER hapen
        throw new RuntimeException("No static context instance");
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationControler=this;
        sStaticContext = getApplicationContext();
        mBus = new Bus();
        mBus.register(this);
        myGlobalArray = new ArrayList<String>();
       // initLoginApi();

     /*   RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);*/

    }

  /*  private void initLoginApi() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        final Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(Constant.MainUrl)
                .client(new OkHttpClient())
                .build();
        sreApi = retrofit.create(PEApi.class);
    }

    public PEApi getSreApi() {
        return sreApi;
    }

    public Bus getBus() {
        return mBus;
    }*/


}

