package com.e.krishianand.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static com.facebook.FacebookSdk.getApplicationContext;

public class LocationUtils {

    private static final int REQUEST_LOCATION = 1;
   static LocationManager locationManager;
    static String latitude, longitude;

    public static void OnGPS(final Activity activity) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                activity.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static String getLocation(Activity activity) {
        if (ActivityCompat.checkSelfPermission(
                activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
           /* Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);*/

                // showLocation.setText("Your Location: " + "\n" + "Latitude: " + latitude + "\n" + "Longitude: " + longitude);


            Location locationGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location locationNetwork =  locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location locationPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if(locationGps != null){
                String lat = String.valueOf(locationGps.getLatitude());
                String longi = String.valueOf(locationGps.getLongitude());

                return  lat+" "+longi;
            }
            else if(locationNetwork != null){
                String lat = String.valueOf(locationNetwork.getLatitude());
                String longi = String.valueOf(locationNetwork.getLongitude());

                return  lat+" "+longi;
            }

            else if(locationPassive != null){
                String lat = String.valueOf(locationPassive.getLatitude());
                String longi = String.valueOf(locationPassive.getLongitude());

                return  lat+" "+longi;
            }
            else {

                Toast.makeText(activity, " Unable to Get Location", Toast.LENGTH_SHORT).show();
                return null;
            }

              //  return latitude + " " + longitude;
            } /*else {
                return "null";
            }
        }*/
return "null";
    }
}
