package com.e.krishianand.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PrefrenceFile {

    private String language;
    private static SharedPreferences prefs;
    private static SharedPreferences.Editor ed;

    private static PrefrenceFile mInstance = null;

    private PrefrenceFile(Context context) {
        super();
    }

    public static PrefrenceFile getInstance(Context mContext) {
        Context ctx = mContext;
        if (mInstance == null) {
            mInstance = new PrefrenceFile(ctx);
        }
        prefs = ctx.getSharedPreferences("MyPrefrence", Context.MODE_PRIVATE);
        ed = prefs.edit();
        return mInstance;
    }

    public void clearData() {
        ed.clear();
        ed.commit();
    }

    public void setBoolean(String key, boolean value) {
        ed.putBoolean(key, value);
        ed.commit();
    }

    public void setInt(String key, int value) {
        ed.putInt(key, value);
        ed.commit();
    }

    public void setString(String key, String value) {
        ed.putString(key, value);
        ed.commit();
    }

    public void clearKeyData(String key){
        ed.remove(key);
        ed.commit();
    }

    public void setEmailsList(String key, List<String> value) {
        Gson gson = new Gson();
        String json = gson.toJson(value);
        ed.putString(key, json);
        ed.commit();
    }

    public ArrayList<String> getEmailsList(String key) {
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        ArrayList<String> arrayList = gson.fromJson(json, type);

        return arrayList;
    }

  /*  public void setCategoryList(String key, ArrayList<HomeResponseResultCategory> value) {
        Gson gson = new Gson();
        String json = gson.toJson(value);
        ed.putString(key, json);
        ed.commit();
    }

    public void setProductList(String key, ArrayList<Category_AllProductResponseAll_products> value) {
        Gson gson = new Gson();
        String json = gson.toJson(value);
        ed.putString(key, json);
        ed.commit();
    }

    public ArrayList<Category_AllProductResponseAll_products> getProductList(String key) {
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<Category_AllProductResponseAll_products>>() {
        }.getType();
        ArrayList<Category_AllProductResponseAll_products> arrayList = gson.fromJson(json, type);
        return arrayList;
    }

    public ArrayList<HomeResponseResultCategory> getCategoryList(String key) {
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<HomeResponseResultCategory>>() {
        }.getType();
        ArrayList<HomeResponseResultCategory> arrayList = gson.fromJson(json, type);
        return arrayList;
    }*/
   /* public void setlist(String key, ArrayList<DraftItem> value) {
        Gson gson = new Gson();
        String json = gson.toJson(value);
        ed.putString(key, json);
        ed.commit();
    }

    public ArrayList<DraftItem> getList(String key) {
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<DraftItem>>() {
        }.getType();
        ArrayList<DraftItem> arrayList = gson.fromJson(json, type);

        return arrayList;
    }


    public void setlist1(String key, List<CartList> value) {
        Gson gson = new Gson();
        String json = gson.toJson(value);
        ed.putString(key, json);
        ed.commit();
    }

    public ArrayList<CartList> getList1(String key) {
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<CartList>>() {
        }.getType();
        ArrayList<CartList> arrayList = gson.fromJson(json, type);

        return arrayList;
    }
*/

    public void saveGCMIDToPreference(String gcmIds) {
        ed.putString("FCM", gcmIds);

        ed.commit();
    }
    public String getGCMFromPreference() {

        String json = prefs.getString("FCM", "");

        return json;
    }



    public void setFloat(String key, float value) {
        ed.putFloat(key, value);
        ed.commit();
    }

    public boolean getBoolean(String key) {
        return prefs.getBoolean(key, false);
    }

    public String getString(String key) {
        return prefs.getString(key, null);
    }

    public int getInt(String key) {
        return prefs.getInt(key, 0);
    }

    public float getFloat(String key) {
        return prefs.getFloat(key, 0.0f);
    }

    public void deleteRecord(String key) {
        ed.remove(key);
        ed.commit();
    }
}
