package com.e.krishianand.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.navigation.NavigationView;

public class UtilityofActivity {

    Activity activity;
    // ImageLoader imageLoader;
    //   DisplayImageOptions options;
    Context mContext;
    private static ImageView mSm_icon, mUsericon, mHearticon, mSearchicon, mBagicon;
    private static TextView mPagename;
    private static LinearLayout mNotifyBag, mWishlist;
    // ImagePipeline imagePipeline;
    NavigationView nav_view;
    TextView mBagcount, mWishcount;
    public int AnimationMode;
    public int LoadMode;
    ProgressDialog progressDialog;
    //  private MyCustomProgressDialog myCustomProgressDialog;


    public UtilityofActivity(AppCompatActivity activity) {
        this.activity = activity;
        mContext = activity.getApplicationContext();
       /* if (imageLoader == null) {
            imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                        activity.getApplicationContext()).threadPoolSize(3)
                        .threadPriority(Thread.NORM_PRIORITY - 2)
                        .threadPoolSize(1)
                        .memoryCacheSize(1500000) // 1.5 Mb
                        .discCacheSize(50000000) // 50 Mb
                        .denyCacheImageMultipleSizesInMemory().build();
                imageLoader.init(config);
            }
        }
        Fresco.initialize(activity.getApplicationContext());*/

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    /*public void setFrescoImageLoader(String imageUrl, final ImageView imageView, int loader, final ProgressBar progressBar) {
        imagePipeline = Fresco.getImagePipeline();


        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(imageUrl))
                .setRequestPriority(Priority.HIGH)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();
        DataSource<CloseableReference<CloseableImage>> dataSource =
                imagePipeline.fetchDecodedImage(imageRequest, mContext);
        //   SimpleDraweeView draweeView =(SimpleDraweeView)imageView;

        setDataSubscriber(mContext, Uri.parse(imageUrl), imageView, progressBar);

    }

    public void setFrescoImageLoader(String imageUrl, final SimpleDraweeView imageView, int loader, final ProgressBar progressBar) {
        imagePipeline = Fresco.getImagePipeline();


        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(imageUrl))
                .setRequestPriority(Priority.HIGH)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();
        DataSource<CloseableReference<CloseableImage>> dataSource =
                imagePipeline.fetchDecodedImage(imageRequest, mContext);
        //   SimpleDraweeView draweeView =(SimpleDraweeView)imageView;

        setDataSubscriber(mContext, Uri.parse(imageUrl), imageView, progressBar);

    }
*/
    /*public void setImageLoader(String imageUrl, final ImageView imageView, int loader, final ProgressBar progressBar) {
        // Logger.e("imageurl",imageUrl);
        int REPEAT_TIME_IN_SECONDS = 60; //repeat every 60 seconds
        Runnable mRunnable;
        Handler mHandler = new Handler();
        mRunnable = new Runnable() {
            @SuppressWarnings("deprecation")
            @Override
            public void run() {
                if (imageLoader!=null) {
                    imageLoader.getDiscCache().clear();
                    imageLoader.getMemoryCache().clear();
                }
            }
        };
        mHandler.postDelayed(mRunnable, REPEAT_TIME_IN_SECONDS * 1000);
        progressBar.getIndeterminateDrawable().setColorFilter(activity.getResources().getColor(R.color.profile_circle), android.graphics.PorterDuff.Mode.SRC_IN);*/

    /*public void setImageLoader(final String imageUrl, final ImageView imageView, final int loader, final ProgressBar progressBar) {
        // Logger.e("imageurl",imageUrl);
*//*
        new AsyncTask<String, String, String>() {

            @Override
            protected String doInBackground(String... params) {*//*
        options = new DisplayImageOptions.Builder()
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .showImageOnLoading(0)
                .showImageOnFail(0)
                .cacheInMemory(false)
                .considerExifParams(true)
                .cacheOnDisc(true).build();
            activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.getIndeterminateDrawable().setColorFilter(activity.getResources().getColor(R.color.profile_circle), PorterDuff.Mode.SRC_IN);

                Glide.with(mContext)
                        .load(imageUrl)
                        .asBitmap()
                        .thumbnail(0.5f)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                progressBar.setVisibility(View.GONE);
                                Bitmap bitmap = getResizedBitmap(resource, 500);
                                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                imageView.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                imageView.setImageDrawable(activity.getResources().getDrawable(0));
                                super.onLoadFailed(e, errorDrawable);
                            }

                            @Override
                            public void onLoadStarted(Drawable placeholder) {
                                imageView.setImageDrawable(activity.getResources().getDrawable(0));
                                super.onLoadStarted(placeholder);
                            }

                            @Override
                            public void onLoadCleared(Drawable placeholder) {
                                imageView.setImageDrawable(activity.getResources().getDrawable(0));
                                super.onLoadCleared(placeholder);
                            }
                        });

                       *//* Glide.with(mContext).load(imageUrl)
                                .thumbnail(0.5f)
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageView);*//*

     *//* if (imageLoader != null) {
                            imageLoader.displayImage(imageUrl, imageView, options, new SimpleImageLoadingListener() {

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    ImageView rowImageView = (ImageView) imageView;
                                    // if (rowImageView.getTag() == position) {
                                    //the row is still correct, set bitmap
                                    //  rowImageView.setImageBitmap(loadedImage);
                                    progressBar.setVisibility(View.GONE);
                                    Bitmap bitmap = getResizedBitmap(loadedImage, 500);
                                    if(bitmap!=null) {
                                        rowImageView.setImageBitmap(bitmap);
                                    }
                                    //    }
                                }
                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    ImageView rowImageView = (ImageView) imageView;
                                   // rowImageView.setImageResource(R.drawable.app_icon);
                                    super.onLoadingFailed(imageUri, view, failReason);
                                }
                            });
                        }*//*
            }
        });
              *//*  return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);*//*

    }*/


/*    public void setImageLoader(String imageUrl, final ImageView imageView, int loader) {
        // Logger.e("imageurl",imageUrl);

        options = new DisplayImageOptions.Builder()
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .showImageOnLoading(0)
                .showImageOnFail(0)
                .cacheInMemory(true)
                .considerExifParams(true)
                .cacheOnDisc(true).build();
        //   progressBar.getIndeterminateDrawable().setColorFilter(activity.getResources().getColor(R.color.profile_circle), android.graphics.PorterDuff.Mode.SRC_IN);
        if (imageLoader != null) {
            imageLoader.displayImage(imageUrl, imageView, options, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    ImageView rowImageView = (ImageView) imageView;
                    // if (rowImageView.getTag() == position) {
                    //the row is still correct, set bitmap
                    //  rowImageView.setImageBitmap(loadedImage);
                    //          progressBar.setVisibility(View.GONE);
                    Bitmap bitmap = getResizedBitmap(loadedImage, 500);
                    if (bitmap != null) {
                        rowImageView.setImageBitmap(bitmap);
                    }
                    //    }
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    ImageView rowImageView = (ImageView) imageView;
                    rowImageView.setImageBitmap(null);
                    super.onLoadingFailed(imageUri, view, failReason);
                }
            });
        }
    }*/

/*

    public void setDataSubscriber(Context context, Uri uri, final ImageView imageView, final ProgressBar progressBar) {
        DataSubscriber dataSubscriber = new BaseDataSubscriber<CloseableReference<CloseableBitmap>>() {
            @Override
            public void onNewResultImpl(
                    DataSource<CloseableReference<CloseableBitmap>> dataSource) {
                if (!dataSource.isFinished()) {
                    return;
                }
                CloseableReference<CloseableBitmap> imageReference = dataSource.getResult();
                if (imageReference != null) {
                    final CloseableReference<CloseableBitmap> closeableReference = imageReference.clone();
                    try {
                        CloseableBitmap closeableBitmap = closeableReference.get();
                        Bitmap bitmap = closeableBitmap.getUnderlyingBitmap();
                        if (bitmap != null && !bitmap.isRecycled()) {
                            //you can use bitmap here
                            progressBar.setVisibility(View.GONE);
                            Bitmap bitmap2 = getResizedBitmap(bitmap, 500);
                            imageView.setImageBitmap(bitmap2);
                            //   bitmap.recycle();
                        }
                    } finally {
                        imageReference.close();
                        closeableReference.close();
                    }
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                Throwable throwable = dataSource.getFailureCause();
                // handle failure
            }
        };
        getBitmap(context, uri, dataSubscriber);
    }
*/

/*
    public void getBitmap(Context context, Uri uri, DataSubscriber dataSubscriber) {
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        imagePipeline.evictFromMemoryCache(uri);
        imagePipeline.evictFromDiskCache(uri);

// combines above two lines
        imagePipeline.evictFromCache(uri);
        ImageRequestBuilder builder = ImageRequestBuilder.newBuilderWithSource(uri);
        //if(width > 0 && height > 0){
        //   builder.setResizeOptions(new ResizeOptions(width, height));
        //     }
        ImageRequest request = builder.build();

        DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(request, context);
        dataSource.subscribe(dataSubscriber, UiThreadExecutorService.getInstance());
    }
*/

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
    public void showProgressDialog() {

        activity.runOnUiThread(new Runnable() {
            public void run() {
                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(activity);
                    progressDialog.getWindow();
                    progressDialog.setCancelable(false);
                }
                progressDialog.setMessage("Please Wait...");
                progressDialog.setIndeterminate(false);

                progressDialog.show();
            }
        });

    }

   /* public void showCutsomProgressDialog(final String title) {
        try {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (myCustomProgressDialog == null) {
                        myCustomProgressDialog = new MyCustomProgressDialog(activity);
                        //   myCustomProgressDialog.setProgressStyle(R.style.CustomAlertDialogStyle);
                        myCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        myCustomProgressDialog.setCancelable(false);
                    }
                    myCustomProgressDialog.setMessage(title);
                    myCustomProgressDialog.show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissCustomDialog() {
        try {
            if (myCustomProgressDialog != null) {
                myCustomProgressDialog.dismiss();
                myCustomProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/
    //main


    //main
    public void progressDialogShow(final String title) {

        try {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(activity);
                        progressDialog.setCancelable(false);
                    }
                    progressDialog.setMessage(title);
                    progressDialog.show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void progresDissmiss() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }


    //Banner Image Uses This function

  /*  public void setDrawableImage(int res, ImageView imageView) {
        options = new DisplayImageOptions.Builder()
                .bitmapConfig(Bitmap.Config.RGB_565)
                .showImageOnLoading(null)
                .showImageOnFail(null)
                .cacheInMemory(true)
                .considerExifParams(true)
                .cacheOnDisc(true).build();
        imageLoader.displayImage("drawable://" + res, imageView, options);

    }*/

    //Main Toast

    public void toast(final String message) {
        Toast.makeText(activity.getBaseContext(),message,
                Toast.LENGTH_SHORT).show();

  /*     try {
           activity.runOnUiThread(new Runnable() {
               @Override
               public void run() {
                   if (message != null && !message.isEmpty()) {
                       View layout = activity.getLayoutInflater().inflate(R.layout.toast,
                               (ViewGroup) activity.findViewById(R.id.toast_layout_root));
                       TextViewLight textViewLight = (TextViewLight) layout.findViewById(R.id.text);
                       textViewLight.setText(message);
                       Toast toast = new Toast(activity);
                       toast.setDuration(Toast.LENGTH_SHORT);
                       toast.setView(layout);
                       toast.show();
                   }
               }
           });
       } catch (Exception ex) {
           ex.printStackTrace();
       }*/
    }

      /*  try {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (message != null && !message.isEmpty())
                        Toast.makeText(activity, message,
                                Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }*/

    // }

   /* public static void toast(final Activity activity, final String message) {
        try {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (message != null && !message.isEmpty())
                        Toast.makeText(activity, message,
                                Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }*/

/*    public LoginResponse getLoginInfo() {
        return new Gson().fromJson(PrefrenceFile.getInstance(mContext).getString(Constant.PREF_KEY_USER_DATA), LoginResponse.class);
    }

    public String getCustomerId() {
        String customer_id = null;
        if (getLoginInfo() != null) {
            LoginResponse loginResponse = getLoginInfo();
          customer_id = loginResponse.getUser_data().getCustomer_id();
        }
        return customer_id;
    }*/



    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    public static GradientDrawable setDrawableColor(Activity activity, int drawableResource, int drawableId, int backgroundColor, int strokeColor) {

        LayerDrawable layerDrawable = (LayerDrawable) activity.getResources()
                .getDrawable(drawableResource);

        GradientDrawable gradientDrawable = (GradientDrawable) layerDrawable
                .findDrawableByLayerId(drawableId);

        if (backgroundColor != 0) {
            gradientDrawable.setColor(activity.getResources().getColor(backgroundColor));
        }
        if (strokeColor != 0) {
            gradientDrawable.setStroke(3, activity.getResources().getColor(strokeColor));
        }
        return gradientDrawable;
    }

    /*  public static String getOrderFormatedDate(String input) {

          // String input_date="01/08/2012";
          try {
              SimpleDateFormat format1 = new SimpleDateFormat("dd MMM, yyyy");
              Date dt1 = null;
              try {
                  dt1 = format1.parse(input);
              } catch (ParseException e) {
                  e.printStackTrace();
              }
              DateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
              String finalDay = format2.format(dt1);
              Logger.e("finalday", finalDay);
              return finalDay;
          } catch (Exception e) {
              e.printStackTrace();
              return null;
          }

      }
  */
    public String getVersionName() {
        PackageInfo pInfo = null;
        try {
            pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        assert pInfo != null;
        String version_Name = pInfo.versionName;
// version_Code = pInfo.versionCode;
// Log.e("version Code","version_Code"+version_Code);//6
        Log.e("version Name", "version_Name" + version_Name);//1.6
        return version_Name;
    }


    public void keyBoardClose(){
        InputMethodManager inputManager = (InputMethodManager)
                activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void createNetErrorDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("You need internet connection for this app. Please turn on mobile network or Wi-Fi in Settings.")
                .setTitle("Unable to connect")
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                activity.startActivity(i);
                            }
                        }
                )
                .setNegativeButton("",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //    MyActivity.this.finish();
                            }
                        }
                );
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static boolean validateEmailAddress(Context context, String emailAddress) {
        String email = emailAddress.trim();

        String emailPattern = "(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))";
        if (email.matches(emailPattern)) {
            return true;
        }
        return false;
    }

  /*  public void LoadImagePicasso1(String imgUrl,ImageView productImageIv){
        Picasso.with(mContext)
                .load(imgUrl)
                .placeholder(R.drawable.loader_image) // optional
                .error(R.drawable.loader_image)         // optional
                .into(productImageIv);
    }
*/

    public void showResponse(String body){
        Log.e("Full response => ",body);
    }


}
