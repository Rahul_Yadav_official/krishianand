package com.e.krishianand;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;

import com.e.krishianand.ui.ccesurvey.CAmeraAct;
import com.e.krishianand.ui.ccesurvey.CCESurvey2Act;
import com.e.krishianand.ui.ccesurvey.CCESurveyActivity;
import com.e.krishianand.ui.editprofile.ChangePassword;
import com.e.krishianand.ui.editprofile.EditProfileActivity;
import com.e.krishianand.ui.login.LoginActivity;
import com.e.krishianand.ui.profile.ProfileActivity;

public class MainActivity extends AppCompatActivity {
    private AppCompatActivity appCompatActivity = this;
    private Context context = this;
    private DrawerLayout drawer;
    private LinearLayout llCceSurvey, llLocalSurvey;
    private RelativeLayout rlTabCCE, rlTabLocal;
    private TextView ccelabel, locallabel;
    private View view1, view2;
    private ImageView sideDrawerIcon;
    private CardView rlCceSurvey;
    private RelativeLayout sideMenuDialoge;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        rlTabCCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ccelabel.setTextColor(getResources().getColor(R.color.red_light));
                locallabel.setTextColor(getResources().getColor(R.color.black));
                view1.setBackgroundColor(getResources().getColor(R.color.red_light));
                view2.setBackgroundColor(getResources().getColor(R.color.black));
                view1.setVisibility(View.VISIBLE);
                view2.setVisibility(View.GONE);

                llCceSurvey.setVisibility(View.VISIBLE);
                llLocalSurvey.setVisibility(View.GONE);
            }
        });

        rlTabLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ccelabel.setTextColor(getResources().getColor(R.color.black));
                locallabel.setTextColor(getResources().getColor(R.color.red_light));
                view1.setBackgroundColor(getResources().getColor(R.color.black));
                view2.setBackgroundColor(getResources().getColor(R.color.red_light));
                view1.setVisibility(View.GONE);
                view2.setVisibility(View.VISIBLE);
                llCceSurvey.setVisibility(View.GONE);
                llLocalSurvey.setVisibility(View.VISIBLE);
            }
        });

        sideDrawerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSideMenuDialog();
            }
        });

        rlCceSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent open= new Intent(MainActivity.this, CCESurvey2Act.class);
                startActivity(open);
            }
        });

        llLocalSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent open= new Intent(MainActivity.this, CCESurvey2Act.class);
                startActivity(open);
            }
        });
    }

    private void init() {
        // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        llCceSurvey = (LinearLayout) findViewById(R.id.llCceSurvey);
        llLocalSurvey = (LinearLayout) findViewById(R.id.llLocalSurvey);
        rlTabCCE = (RelativeLayout) findViewById(R.id.rlTabCCE);
        rlTabLocal = (RelativeLayout) findViewById(R.id.rlTabLocal);
        ccelabel = (TextView) findViewById(R.id.ccelabel);
        locallabel = (TextView) findViewById(R.id.locallabel);
        view1 = (View) findViewById(R.id.view1);
        view2 = (View) findViewById(R.id.view2);
        sideDrawerIcon = (ImageView) findViewById(R.id.sideDrawerIcon);
        rlCceSurvey = (CardView) findViewById(R.id.rlCceSurvey);
        //llLocalSurvey = (LinearLayout) findViewById(R.id.llLocalSurvey);
        sideMenuDialoge = (RelativeLayout) findViewById(R.id.side_menu_dg);
    }

    private void openSideMenuDialog() {

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.side_menu_dialog);

       // sideMenuDialoge.setVisibility(View.VISIBLE);


      /*  TextView userNameSm = findViewById(R.id.user_name_sm);
        userNameSm.setText(PrefrenceFile.getInstance(context).getString("name"));*/

        RelativeLayout rlViewProfile = (RelativeLayout) dialog.findViewById(R.id.rlViewProfile);
        RelativeLayout rlEditProfile = (RelativeLayout) dialog.findViewById(R.id.rlEditProfile);
        RelativeLayout  rlChangePass = (RelativeLayout) dialog.findViewById(R.id.rl_change_pass);
        RelativeLayout rlLogout = (RelativeLayout) dialog.findViewById(R.id.rlLogout);
        ImageView ic_close = (ImageView) dialog.findViewById(R.id.closeDialog);
        rlViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent open = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(open);
                dialog.dismiss();
            }
        });

        rlEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  sideMenuDialoge.setVisibility(View.GONE);
                Intent open = new Intent(MainActivity.this, EditProfileActivity.class);
                startActivity(open);
                dialog.dismiss();
            }
        });

        rlChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent open = new Intent(MainActivity.this, ChangePassword.class);
                startActivity(open);
                dialog.dismiss();
            }
        });
        rlLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent open = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(open);
                finish();
                dialog.dismiss();
            }
        });
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        //  wmlp.gravity = Gravity.CENTER | Gravity.RIGHT;
        //  wmlp.x = -500; //x position
        //  wmlp.gravity = Gravity.BOTTOM;
        window.getAttributes().windowAnimations = R.style.DialogAnimation;
        //  wmlp.x = -500; //x position
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null && item.getItemId() == android.R.id.home) {
            if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                drawer.closeDrawer(Gravity.RIGHT);
            } else {
                drawer.openDrawer(Gravity.RIGHT);
            }
        }
        return false;
    }
}