package com.e.krishianand.ui.model.response;

public class SignUpResponse {
    private int Status;
    private String Message;
    private boolean Error;
    private SignUpResponsePayload Payload;

    public int getStatus() {
        return this.Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return this.Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getError() {
        return this.Error;
    }

    public void setError(boolean Error) {
        this.Error = Error;
    }

    public SignUpResponsePayload getPayload() {
        return this.Payload;
    }

    public void setPayload(SignUpResponsePayload Payload) {
        this.Payload = Payload;
    }
}
