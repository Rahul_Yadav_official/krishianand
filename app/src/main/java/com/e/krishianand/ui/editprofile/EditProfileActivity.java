package com.e.krishianand.ui.editprofile;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.e.krishianand.MainActivity;
import com.e.krishianand.R;
import com.e.krishianand.api.RetrofitApi;
import com.e.krishianand.ui.login.LoginActivity;
import com.e.krishianand.ui.model.response.EditProfileResponse;
import com.e.krishianand.ui.model.response.LoginResponse;
import com.e.krishianand.util.AppConstant;
import com.e.krishianand.util.PrefrenceFile;
import com.e.krishianand.util.UtilityofActivity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backImg, edt_pic;
    public static final int GET_FROM_GALLERY = 100;
    private TextView continue_ed;
    private Context context = this;
    private EditText fullName, userNAme, mobilNo, emailId;
    private Spinner genderSpinner;
    private String authToken;
    private UtilityofActivity utilityofActivity;
    private AppCompatActivity appCompatActivity = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        init();
        setProfile();
        authToken = PrefrenceFile.getInstance(context).getString("authtoken");

    }


    public void init() {
        utilityofActivity = new UtilityofActivity(appCompatActivity);
        backImg = (ImageView) findViewById(R.id.backImg);
        edt_pic = findViewById(R.id.edt_profile_pic);
        continue_ed = findViewById(R.id.continue_ed);
     //   fullName = findViewById(R.id.full_name_ep);
        userNAme = findViewById(R.id.user_name_ep);
        mobilNo = findViewById(R.id.mobile_no_ep);
        emailId = findViewById(R.id.email_id_ep);
       //genderSpinner = findViewById(R.id.gender_spinner_ep);


        backImg.setOnClickListener(this);
        edt_pic.setOnClickListener(this);
        continue_ed.setOnClickListener(this);
    }


    private void setProfile() {
        String user_name = PrefrenceFile.getInstance(context).getString("name");
        String mobile_no = PrefrenceFile.getInstance(context).getString("mobile");
        String email_id = PrefrenceFile.getInstance(context).getString("email");


        userNAme.setText(user_name);
        mobilNo.setText(mobile_no);
        emailId.setText(email_id);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.backImg: {
                finish();
            }
                break;
            case R.id.edt_profile_pic: {
             //   startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY);
            }
                break;
            case R.id.continue_ed: {
                edit_profile_api();
            }
            break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Detects request codes
        if (requestCode == GET_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                Log.e("image1 ", String.valueOf(bitmap));
//                AppPrefrences.getInstance(UserProfileAct.this).saveDataToPrefs("user_image", String.valueOf(bitmap));
//                String user_image = "" + AppPrefrences.getInstance(UserProfileAct.this).getDataFromPrefs("user_image");
//                Log.e("image 2", user_image);
//                userImage.setImageBitmap(BitmapFactory.decodeFile(user_image));

                edt_pic.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void edit_profile_api() {

        Map<String, String> params = new HashMap<String, String>();
        params.put("authtoken", authToken);
        params.put("name", fullName.getText().toString());
        params.put("contact_num", mobilNo.getText().toString());
        params.put("email", emailId.getText().toString());
        utilityofActivity.showProgressDialog();

        RetrofitApi.getTickerAppObject().EditProfileApi(params).enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                utilityofActivity.dismissProgressDialog();
                EditProfileResponse data = (EditProfileResponse) response.body();
                if (data.getStatus() == 1) {
                    PrefrenceFile.getInstance(context).setBoolean(AppConstant.isLogin, true);
                    PrefrenceFile.getInstance(context).setString("name", data.getPayload().getName());
                    PrefrenceFile.getInstance(context).setString("mobile", data.getPayload().getContactNum());
                    PrefrenceFile.getInstance(context).setString("email", data.getPayload().getEmail());
//                    PrefrenceFile.getInstance(context).setString("imageurl", data.getPayload().getImage().toString());

                    utilityofActivity.toast(data.getMessage());
                    Intent open = new Intent(EditProfileActivity.this, MainActivity.class);
                    startActivity(open);
                    finish();
                } else {
                    utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                utilityofActivity.dismissProgressDialog();
                Log.e("Krishi", "fail");
                t.printStackTrace();
            }
        });
    }
}
