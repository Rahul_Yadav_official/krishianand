package com.e.krishianand.ui.model.response;

public class SignUpResponsePayload {
    private int user_id;
    private String authtoken;
    private SignUpResponsePayloadUser_data user_data;

    public int getUser_id() {
        return this.user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getAuthtoken() {
        return this.authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public SignUpResponsePayloadUser_data getUser_data() {
        return this.user_data;
    }

    public void setUser_data(SignUpResponsePayloadUser_data user_data) {
        this.user_data = user_data;
    }
}
