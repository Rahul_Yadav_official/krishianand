package com.e.krishianand.ui.model.response;

public class SignUpResponsePayloadUser_data {
    private String contact_num;
    private String image_url;
    private String name;
    private String email;

    public String getContact_num() {
        return this.contact_num;
    }

    public void setContact_num(String contact_num) {
        this.contact_num = contact_num;
    }

    public String getImage_url() {
        return this.image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
