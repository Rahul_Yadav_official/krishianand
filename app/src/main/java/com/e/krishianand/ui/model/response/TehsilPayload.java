
package com.e.krishianand.ui.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TehsilPayload {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("district_id")
    @Expose
    private String districtId;
    @SerializedName("tehsil_name")
    @Expose
    private String tehsilName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getTehsilName() {
        return tehsilName;
    }

    public void setTehsilName(String tehsilName) {
        this.tehsilName = tehsilName;
    }

}
