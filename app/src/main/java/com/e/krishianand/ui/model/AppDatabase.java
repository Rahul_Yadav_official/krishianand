package com.e.krishianand.ui.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {CCETable.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract TaskDao taskDao();
}
