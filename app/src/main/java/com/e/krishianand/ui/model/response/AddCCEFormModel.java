
package com.e.krishianand.ui.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddCCEFormModel {

    @SerializedName("Error")
    @Expose
    private Boolean error;
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Payload")
    @Expose
    private AddCCEFormApiPayload addCCEFormApiPayload;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AddCCEFormApiPayload getAddCCEFormApiPayload() {
        return addCCEFormApiPayload;
    }

    public void setAddCCEFormApiPayload(AddCCEFormApiPayload addCCEFormApiPayload) {
        this.addCCEFormApiPayload = addCCEFormApiPayload;
    }

}
