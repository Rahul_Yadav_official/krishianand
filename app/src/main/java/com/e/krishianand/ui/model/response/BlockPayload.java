
package com.e.krishianand.ui.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BlockPayload {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tehsil_id")
    @Expose
    private String tehsilId;
    @SerializedName("girdawar_circle_name")
    @Expose
    private String girdawarCircleName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTehsilId() {
        return tehsilId;
    }

    public void setTehsilId(String tehsilId) {
        this.tehsilId = tehsilId;
    }

    public String getGirdawarCircleName() {
        return girdawarCircleName;
    }

    public void setGirdawarCircleName(String girdawarCircleName) {
        this.girdawarCircleName = girdawarCircleName;
    }

}
