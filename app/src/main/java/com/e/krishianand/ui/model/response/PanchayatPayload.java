
package com.e.krishianand.ui.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PanchayatPayload {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("girdawar_id")
    @Expose
    private String girdawarId;
    @SerializedName("patwar_halka_name")
    @Expose
    private String patwarHalkaName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGirdawarId() {
        return girdawarId;
    }

    public void setGirdawarId(String girdawarId) {
        this.girdawarId = girdawarId;
    }

    public String getPatwarHalkaName() {
        return patwarHalkaName;
    }

    public void setPatwarHalkaName(String patwarHalkaName) {
        this.patwarHalkaName = patwarHalkaName;
    }

}
