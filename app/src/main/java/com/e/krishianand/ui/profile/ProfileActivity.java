package com.e.krishianand.ui.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.e.krishianand.R;
import com.e.krishianand.util.AppConstant;
import com.e.krishianand.util.PrefrenceFile;

public class ProfileActivity extends AppCompatActivity {
    private ImageView backImg;
    private Context context = this;
    private TextView tvFullName, tvUserName, tvMobileNo, tvEmailId, tvGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        init();
        setProfile();

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void init() {
        backImg = (ImageView) findViewById(R.id.backImg);
      //  tvFullName = (TextView) findViewById(R.id.full_name_mp);
        tvUserName = (TextView) findViewById(R.id.user_name_mp);
        tvMobileNo = (TextView) findViewById(R.id.mobile_no_mp);
        tvEmailId = (TextView) findViewById(R.id.email_id_mp);
       // tvGender = (TextView) findViewById(R.id.gender_mp);
    }

    private void setProfile() {
        String user_name = PrefrenceFile.getInstance(context).getString("name");
        String mobile_no = PrefrenceFile.getInstance(context).getString("mobile");
        String email_id = PrefrenceFile.getInstance(context).getString("email");


        tvUserName.setText(user_name);
        tvMobileNo.setText(mobile_no);
        tvEmailId.setText(email_id);

    }
}