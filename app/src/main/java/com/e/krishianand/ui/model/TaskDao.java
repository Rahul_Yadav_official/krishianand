package com.e.krishianand.ui.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Maybe;

@Dao
public interface TaskDao {

    @Query("SELECT * FROM cce")
    List<CCETable> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Maybe<Long> insert(CCETable cceTable);

    @Delete
    void delete(CCETable cceTable);

    @Update
    void update(CCETable cceTable);
}
