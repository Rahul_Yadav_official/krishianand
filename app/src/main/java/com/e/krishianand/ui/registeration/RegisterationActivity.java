package com.e.krishianand.ui.registeration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.e.krishianand.MainActivity;
import com.e.krishianand.R;
import com.e.krishianand.api.RetrofitApi;
import com.e.krishianand.ui.login.LoginActivity;
import com.e.krishianand.ui.model.response.SignUpResponse;
import com.e.krishianand.util.AppConstant;
import com.e.krishianand.util.PrefrenceFile;
import com.e.krishianand.util.UtilityofActivity;
import com.facebook.login.Login;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterationActivity extends AppCompatActivity {

    private ImageView arrowimage;
    private Context context = this;
    private AppCompatActivity appCompatActivity = this;
    private UtilityofActivity utilityofActivity;
    private EditText nameEdt,emailEdt,passwordEdt,phoneEdt,cnfrmPass;
    private Button btRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);

        init();

        arrowimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()){
                    callSignUpApi();
                }
            }
        });
    }

    private  void init(){
        utilityofActivity = new UtilityofActivity(appCompatActivity);
        arrowimage = (ImageView) findViewById(R.id.arrowimage);
        nameEdt = (EditText) findViewById(R.id.nameEdt);
        emailEdt = (EditText) findViewById(R.id.emailEdt);
        passwordEdt = (EditText) findViewById(R.id.passwordEdt);
        cnfrmPass = (EditText) findViewById(R.id.cnfrm_passwordEdt);
        phoneEdt = (EditText) findViewById(R.id.phoneEdt);
        btRegister = (Button) findViewById(R.id.btRegister);
    }

    public boolean validation() {
        if (nameEdt.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the name");
        }else if(emailEdt.getText().toString().trim().isEmpty()){
            utilityofActivity.toast("Please enter the email id");
        }else if(!utilityofActivity.validateEmailAddress(context,emailEdt.getText().toString())){
            utilityofActivity.toast("Please enter the valid email id");
        }else if(phoneEdt.getText().toString().trim().isEmpty()){
            utilityofActivity.toast("Please enter the phone no");
        }else if(phoneEdt.getText().toString().length()<10){
            utilityofActivity.toast("Please enter the 10 digit phone no");
        }else if(passwordEdt.getText().toString().trim().isEmpty()){
            utilityofActivity.toast("Please enter the password");
        }else if(cnfrmPass.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter confirm password");
        }else if(!passwordEdt.getText().toString().trim().equals(cnfrmPass.getText().toString().trim())) {
            utilityofActivity.toast("Password confirmation doesn't match");
        cnfrmPass.setError("The password confirmation does not match");
        }
        else {
            return true;
        }
        return false;
    }


    private void callSignUpApi() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", nameEdt.getText().toString());
        params.put("email", emailEdt.getText().toString());
        params.put("contact_num", phoneEdt.getText().toString());
        params.put("password",passwordEdt.getText().toString() );
        utilityofActivity.showProgressDialog();
        RetrofitApi.getTickerAppObject().signupApi(params).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                utilityofActivity.dismissProgressDialog();
                SignUpResponse data = (SignUpResponse) response.body();
                if (data.getStatus() == 1) {
                PrefrenceFile.getInstance(context).setBoolean(AppConstant.isLogin, true);
                PrefrenceFile.getInstance(context).setString("name", data.getPayload().getUser_data().getName());
                PrefrenceFile.getInstance(context).setString("mobile", data.getPayload().getUser_data().getContact_num());
                PrefrenceFile.getInstance(context).setString("email", data.getPayload().getUser_data().getEmail());
                PrefrenceFile.getInstance(context).setString("imageurl", data.getPayload().getUser_data().getImage_url());
                PrefrenceFile.getInstance(context).setString("authtoken", data.getPayload().getAuthtoken());
                    utilityofActivity.toast(data.getMessage());
                Intent open = new Intent(RegisterationActivity.this, LoginActivity.class);
                startActivity(open);
                finish();
                }else {
                    utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                utilityofActivity.dismissProgressDialog();
                Log.e("Krishi", "fail");
                t.printStackTrace();
            }
        });
    }
}