package com.e.krishianand.ui.ccesurvey;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.e.krishianand.R;

import java.io.IOException;

public class CAmeraAct extends AppCompatActivity {

    Button img_btn;
    ImageView img_prv;
    Context context = this;
private  static final int REQUEST_IMG_CAPTURE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c_amera);

        img_btn = findViewById(R.id.img_btn);
        img_prv = findViewById(R.id.camera_preview);
        //img_txt = findViewById(R.id.txt_img);


        // Create an instance of Camera
 //       mCamera = getCameraInstance();

        /*// Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);*/


        img_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent imageTakeIntent  = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if(imageTakeIntent.resolveActivity(getPackageManager()) != null){
                    startActivityForResult(imageTakeIntent, REQUEST_IMG_CAPTURE);
                }
            }
        });
    }
/*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_IMG_CAPTURE ){
        Bundle extras = data.getExtras();
        Bitmap bitmap = (Bitmap) extras.get("data");
        //img_prv.setImageBitmap(bitmap);

            ExifInterface ei = null;
            try {
                ei = new ExifInterface(String.valueOf(bitmap));
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap rotatedBitmap = null;
            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bitmap, 90);

                    img_prv.setImageBitmap(rotatedBitmap);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bitmap, 180);
                    img_prv.setImageBitmap(rotatedBitmap);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bitmap, 270);
                    img_prv.setImageBitmap(rotatedBitmap);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bitmap;
                    img_prv.setImageBitmap(rotatedBitmap);
            }

    }}*/

  /*  public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }*/

    /* *//** A safe way to get an instance of the Camera object. *//*
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable


    }


        private boolean checkCameraHardware(Context context){
            if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                // this device has a camera
                return true;
            } else {
                // no camera on this device
                return false;
            }

        }*/
}