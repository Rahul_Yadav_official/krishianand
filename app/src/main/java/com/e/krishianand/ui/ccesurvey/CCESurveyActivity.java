package com.e.krishianand.ui.ccesurvey;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.e.krishianand.R;
import com.e.krishianand.api.RetrofitApi;
import com.e.krishianand.ui.model.response.DistrictModel;
import com.e.krishianand.ui.model.response.TehsilModel;
import com.e.krishianand.util.UtilityofActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CCESurveyActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private ImageView backImg;
    private Spinner stateSpnr, districtSpnr, tehsilSpnr;
    private List<String> stateList, state_id;
    private UtilityofActivity utilityofActivity;
    private AppCompatActivity appCompatActivity = this;
    private Context context = this;
    private DistrictModel districtModel;
    private TehsilModel tehsilModel;
    private String districtId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c_c_e_survey);


        init();


        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void init() {
        utilityofActivity = new UtilityofActivity(appCompatActivity);
        backImg = (ImageView) findViewById(R.id.backImg);
        stateList = new ArrayList<String>();
        state_id = new ArrayList<>();
        stateSpnr = findViewById(R.id.state_spnr);
        districtSpnr = findViewById(R.id.district_spnr);
        tehsilSpnr = findViewById(R.id.tehdil_spnr);



        stateSpnr.setOnItemSelectedListener(this);
        districtSpnr.setOnItemSelectedListener(this);
        tehsilSpnr.setOnItemSelectedListener(this);

        stateList.add("Uttar Pradesh");
        stateList.add("Punjab");
        stateList.add("Rajasthan");
        stateList.add("Haryana");
        stateList.add("Maharashtra");
        stateList.add("Gujrat");
        stateList.add("Assam");
        stateList.add("Karnataka");
        stateList.add("Nagaland");
        stateList.add("Bihar");

        state_id.add("9");
        state_id.add("10");
        state_id.add("30");
        state_id.add("18");
        state_id.add("24");
        state_id.add("17");
        state_id.add("13");
        state_id.add("21");
        state_id.add("28");
        state_id.add("14");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stateList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        stateSpnr.setAdapter(dataAdapter);

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinner = (Spinner) parent;

        if (spinner.getId() == R.id.state_spnr) {

            String stateID = state_id.get(position);
            //Toast.makeText(this, " state id " + stateID, Toast.LENGTH_SHORT).show();
            district_api(stateID);
        }

        if (spinner.getId() == R.id.district_spnr) {
            districtId = districtModel.getDistrictPayload().get(position).getId().toString();
            //String stateID = state_id.get(position);
         //   Toast.makeText(this, " state id " + districtId, Toast.LENGTH_SHORT).show();
            tehsil_api(districtId);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void district_api(String state_id) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("state_id", state_id);
        utilityofActivity.showProgressDialog();

        RetrofitApi.getTickerAppObject().DistrictApi(params).enqueue(new Callback<DistrictModel>() {
            @Override
            public void onResponse(Call<DistrictModel> call, Response<DistrictModel> response) {
                utilityofActivity.dismissProgressDialog();
                districtModel = (DistrictModel) response.body();
                if (districtModel.getStatus() == 1) {

                    populateDistrictSpinner();

                } else {
                    //  utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<DistrictModel> call, Throwable t) {
                utilityofActivity.dismissProgressDialog();
                Log.e("Krishi", "fail");
                t.printStackTrace();
            }
        });
    }


    private void populateDistrictSpinner() {
        List<String> districtList = new ArrayList<>();

        for (int i = 0; i < districtModel.getDistrictPayload().size(); i++) {
            districtList.add(districtModel.getDistrictPayload().get(i).getDistrictName());
        }

        //districtList = data.getDistrictPayload().get().getDistrictName();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, districtList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        districtSpnr.setAdapter(dataAdapter);

    }


    private void tehsil_api(String district_id) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("district_id", district_id);
        utilityofActivity.showProgressDialog();

        RetrofitApi.getTickerAppObject().TehsilApi(params).enqueue(new Callback<TehsilModel>() {
            @Override
            public void onResponse(Call<TehsilModel> call, Response<TehsilModel> response) {
                utilityofActivity.dismissProgressDialog();
                tehsilModel = (TehsilModel) response.body();
                if (tehsilModel.getStatus() == 1) {

                    populateTehsilSpinner();

                } else {
                    //  utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TehsilModel> call, Throwable t) {
                utilityofActivity.dismissProgressDialog();
                //    Log.e("Krishi", "fail");
                t.printStackTrace();
            }
        });
    }


    private void populateTehsilSpinner() {
        List<String> tehsilList = new ArrayList<>();

        for (int i = 0; i < tehsilModel.getTehsilPayload().size(); i++) {
            tehsilList.add(tehsilModel.getTehsilPayload().get(i).getTehsilName());
        }

        //districtList = data.getDistrictPayload().get().getDistrictName();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tehsilList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        tehsilSpnr.setAdapter(dataAdapter);

    }
}