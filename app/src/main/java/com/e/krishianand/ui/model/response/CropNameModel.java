
package com.e.krishianand.ui.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CropNameModel {

    @SerializedName("Error")
    @Expose
    private Boolean error;
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Payload")
    @Expose
    private List<CropNamePayload> cropNamePayload = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<CropNamePayload> getCropNamePayload() {
        return cropNamePayload;
    }

    public void setCropNamePayload(List<CropNamePayload> cropNamePayload) {
        this.cropNamePayload = cropNamePayload;
    }

}
