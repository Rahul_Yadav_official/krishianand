package com.e.krishianand.ui.ccesurvey;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.e.krishianand.R;
import com.e.krishianand.api.RetrofitApi;
import com.e.krishianand.ui.model.response.AddCCEFormModel;
import com.e.krishianand.ui.model.response.BlockModel;
import com.e.krishianand.ui.model.response.CropNameModel;
import com.e.krishianand.ui.model.response.CropSeasonModel;
import com.e.krishianand.ui.model.response.DistrictModel;
import com.e.krishianand.ui.model.response.PanchayatModel;
import com.e.krishianand.ui.model.response.TehsilModel;
import com.e.krishianand.util.ImageUtils;
import com.e.krishianand.util.LocationUtils;
import com.e.krishianand.util.PrefrenceFile;
import com.e.krishianand.util.UtilityofActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CCESurvey2Act extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private ImageView backImg;
    private Spinner stateSpnr, districtSpnr, tehsilSpnr, cropSeasonSpnr, cropNameSpnr, blockSpnr, panchayatSpnr, cceTypeSpnr, farmerTypeSpnr, cropConditionSpnr, stressSpnr, shapeOfCceSpnr;
    private List<String> stateList, state_id, cceType, farmerType, cropCondition, stress, shapeOfCCE;
    private UtilityofActivity utilityofActivity;
    private EditText insuranceCmpny, villageName, farmerName, randomNumb, kasraNumb, experimentId, wetWeight, totalPlotArea, dimensionCce, moisturePrcntg, govtOffrName, govtOffrMobNumb, designation, remark;
    private LinearLayout saveCCE, draftCCe, imgFieldDistLl, imgFieldCorLl, imgPlotFarmLl, imgSurrFieldLl, sImgFieldDistLl, sImgFieldCorLl, sImgPlotFarmLl, sImgSurrFieldLl;
    private ImageView imgFieldDist_iv, imgFieldCor_iv, imgPlotFarm_iv, imgSurrField_iv;
    private TextView imgFieldDist_tv, imgFieldCor_tv, imgPlotFarm_tv, imgSurrField_tv,lat1,long1,lat2,long2,lat3,long3,lat4,long4;
    private Button cImgFieldDist_btn, cImgFieldCor_btn, cImgPlotFarm_btn, cImgSurrField_btn;
    private AppCompatActivity appCompatActivity = this;
    private Context context = this;
    private DistrictModel districtModel;
    private CropSeasonModel cropSeasonModel;
    private CropNameModel cropNameModel;
    private PanchayatModel panchayatModel;
    private AddCCEFormModel addCCEFormModel;
    private TehsilModel tehsilModel;
    private BlockModel blockModel;
    LocationManager locationManager;
    String latitude, longitude;
    private String districtId, tehsilId, blockId;
    private static final int REQUEST_IMG_CAPTURE1 = 101;
    private static final int REQUEST_IMG_CAPTURE2 = 102;
    private static final int REQUEST_IMG_CAPTURE3 = 103;
    private static final int REQUEST_IMG_CAPTURE4 = 104;
    private static final int REQUEST_CODE1 = 1;
    private Boolean isGps =false;
    private String authToken;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c_c_e_survey2);
        authToken = PrefrenceFile.getInstance(context).getString("authtoken");
        init();


        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        crop_season_api();
        crop_name_api();
            requestPermission(REQUEST_CODE1);
            onGPS();

    }


    private void init() {
        utilityofActivity = new UtilityofActivity(appCompatActivity);
        backImg = (ImageView) findViewById(R.id.backImg);
        stateList = new ArrayList<String>();
        state_id = new ArrayList<>();
        cceType = new ArrayList<>();
        farmerType = new ArrayList<>();
        cropCondition = new ArrayList<>();
        stress = new ArrayList<>();
        shapeOfCCE = new ArrayList<>();
        stateSpnr = findViewById(R.id.state_spnr);
        districtSpnr = findViewById(R.id.district_spnr);
        tehsilSpnr = findViewById(R.id.tehdil_spnr);
        cropSeasonSpnr = findViewById(R.id.crop_season_spnr);
        cropNameSpnr = findViewById(R.id.crop_name_spnr);
        blockSpnr = findViewById(R.id.block_spnr);
        farmerTypeSpnr = findViewById(R.id.farmer_type_spnr);
        stressSpnr = findViewById(R.id.stress_spnr);
        cropConditionSpnr = findViewById(R.id.crop_condition_spnr);
        shapeOfCceSpnr = findViewById(R.id.shape_of_cce_spnr);
        cceTypeSpnr = findViewById(R.id.cce_type_spnr);
        shapeOfCceSpnr = findViewById(R.id.shape_of_cce_spnr);
        saveCCE = findViewById(R.id.layout_btn_save_cce);
        draftCCe = findViewById(R.id.layout_btn_draft_cce);
        insuranceCmpny = findViewById(R.id.insurance_company_edt);
        villageName = findViewById(R.id.village_ed_text);
        farmerName = findViewById(R.id.farmer_name_ed_text);
        randomNumb = findViewById(R.id.random_numb_ed_txt);
        kasraNumb = findViewById(R.id.khasra_numb_ed_txt);
        experimentId = findViewById(R.id.experiment_ed_txt);
        wetWeight = findViewById(R.id.wet_weight_ed_txt);
        totalPlotArea = findViewById(R.id.plot_area_ed_txt);
        dimensionCce = findViewById(R.id.dimension_of_cce_ed_txt);
        moisturePrcntg = findViewById(R.id.moisture_percentage_ed_txt);
        govtOffrName = findViewById(R.id.govt_officer_name_rd_txt);
        govtOffrMobNumb = findViewById(R.id.govt_mobile_numb_rd_txt);
        designation = findViewById(R.id.designation_ed_txt);
        remark = findViewById(R.id.remark_ed_txt);


        panchayatSpnr = findViewById(R.id.panchayat_spnr);
        imgFieldDistLl = findViewById(R.id.img_field_distance_ll);
        imgFieldCorLl = findViewById(R.id.img_field_corner_ll);
        imgPlotFarmLl = findViewById(R.id.img_plot_farmer_ll);
        imgSurrFieldLl = findViewById(R.id.img_surround_field_ll);
        sImgFieldDistLl = findViewById(R.id.show_img_field_distance_ll);
        sImgFieldCorLl = findViewById(R.id.show_img_field_corner_ll);
        sImgPlotFarmLl = findViewById(R.id.show_img_plot_farmer_ll);
        sImgSurrFieldLl = findViewById(R.id.show_img_field_surround_ll);
        imgFieldDist_iv = findViewById(R.id.image_view_distance);
        imgFieldCor_iv = findViewById(R.id.image_view_corner);
        imgPlotFarm_iv = findViewById(R.id.image_view_plot_farmer);
        imgSurrField_iv = findViewById(R.id.image_view_surround);
        imgFieldDist_tv = findViewById(R.id.img_field_distance_btn);
        imgFieldCor_tv = findViewById(R.id.img_field_corner_btn);
        imgPlotFarm_tv = findViewById(R.id.img_plot_farmer_btn);
        imgSurrField_tv = findViewById(R.id.img_surround_field_btn);
        cImgFieldDist_btn = findViewById(R.id.btn_change_distance_img);
        cImgFieldCor_btn = findViewById(R.id.btn_change_corner_img);
        cImgPlotFarm_btn = findViewById(R.id.btn_change_plot_farmer_img);
        cImgSurrField_btn = findViewById(R.id.btn_change_surround_img);
        lat1 = findViewById(R.id.lat1);
        long1 = findViewById(R.id.long1);
        lat2= findViewById(R.id.lat2);
        long2 = findViewById(R.id.long2);
        lat3 = findViewById(R.id.lat3);
        long3 = findViewById(R.id.long3);
        lat4 = findViewById(R.id.lat4);
        long4 = findViewById(R.id.long4);



        stateSpnr.setOnItemSelectedListener(this);
        districtSpnr.setOnItemSelectedListener(this);
        tehsilSpnr.setOnItemSelectedListener(this);
        cropSeasonSpnr.setOnItemSelectedListener(this);
        cropNameSpnr.setOnItemSelectedListener(this);
        cceTypeSpnr.setOnItemSelectedListener(this);
        cropConditionSpnr.setOnItemSelectedListener(this);
        blockSpnr.setOnItemSelectedListener(this);
        panchayatSpnr.setOnItemSelectedListener(this);
        imgFieldDist_tv.setOnClickListener(this);
        imgFieldCor_tv.setOnClickListener(this);
        imgPlotFarm_tv.setOnClickListener(this);
        imgSurrField_tv.setOnClickListener(this);
        cImgFieldDist_btn.setOnClickListener(this);
        cImgFieldCor_btn.setOnClickListener(this);
        cImgPlotFarm_btn.setOnClickListener(this);
        cImgSurrField_btn.setOnClickListener(this);
        saveCCE.setOnClickListener(this);
        draftCCe.setOnClickListener(this);

        stateList.add("Uttar Pradesh");
        stateList.add("Punjab");
        stateList.add("Rajasthan");
        stateList.add("Haryana");
        stateList.add("Maharashtra");
        stateList.add("Gujrat");
        stateList.add("Assam");
        stateList.add("Karnataka");
        stateList.add("Nagaland");
        stateList.add("Bihar");
        //cceType,farmerType,cropCondition,stress,shapeOfCCE
        //  cceType.add("");
        farmerType.add("Select Farmer Type");
        farmerType.add("Loanee");
        farmerType.add("Non-Loanee");


        cceType.add("Select CCE Type");
        cceType.add("Single Picking");
        cceType.add("Multi Picking");


        cropCondition.add("Select Crop Condition");
        cropCondition.add("Good");
        cropCondition.add("Bad");
        cropCondition.add("Average");


        stress.add("Select Stress");
        stress.add("Water Stress");
        stress.add("Dieses");
        stress.add("Neutriant stress");
        stress.add("No Stress");


        state_id.add("9");
        state_id.add("10");
        state_id.add("30");
        state_id.add("18");
        state_id.add("24");
        state_id.add("17");
        state_id.add("13");
        state_id.add("21");
        state_id.add("28");
        state_id.add("14");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stateList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        stateSpnr.setAdapter(dataAdapter);


        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cceType);

        // Drop down layout style - list view with radio button
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        cceTypeSpnr.setAdapter(dataAdapter2);


        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stress);

        // Drop down layout style - list view with radio button
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        stressSpnr.setAdapter(dataAdapter3);

        ArrayAdapter<String> dataAdapter4 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, farmerType);

        // Drop down layout style - list view with radio button
        dataAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        farmerTypeSpnr.setAdapter(dataAdapter4);

        ArrayAdapter<String> dataAdapter5 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, shapeOfCCE);

        // Drop down layout style - list view with radio button
        dataAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        shapeOfCceSpnr.setAdapter(dataAdapter5);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinner = (Spinner) parent;

        if (spinner.getId() == R.id.state_spnr) {

            String stateID = state_id.get(position);
            //Toast.makeText(this, " state id " + stateID, Toast.LENGTH_SHORT).show();
            district_api(stateID);
        }

        if (spinner.getId() == R.id.district_spnr) {
            //  if(position >=1){
            if (districtModel.getDistrictPayload().size() > 0) {
                Log.e("district model ", String.valueOf(districtModel.getDistrictPayload().size()));
                Log.e(" pos ", String.valueOf(position));

                districtId = districtModel.getDistrictPayload().get(position).getId().toString();
                //String stateID = state_id.get(position);
                //   Toast.makeText(this, " state id " + districtId, Toast.LENGTH_SHORT).show();
                tehsil_api(districtId);
            }
            // }
        }
        if (spinner.getId() == R.id.tehdil_spnr) {
            if (tehsilModel.getTehsilPayload().size() >= 1) {
                tehsilId = tehsilModel.getTehsilPayload().get(position).getId().toString();

                block_api(tehsilId);
            }
        }

        if (spinner.getId() == R.id.block_spnr) {
            if (blockModel.getBlockPayload().size() >= 1) {
                blockId = blockModel.getBlockPayload().get(position).getId().toString();

                panchayaat_api(blockId);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void crop_season_api() {

        //   utilityofActivity.showProgressDialog();

        RetrofitApi.getTickerAppObject().CropSeasonApi().enqueue(new Callback<CropSeasonModel>() {
            @Override
            public void onResponse(Call<CropSeasonModel> call, Response<CropSeasonModel> response) {
                //        utilityofActivity.dismissProgressDialog();
                cropSeasonModel = (CropSeasonModel) response.body();
                if (cropSeasonModel.getStatus() == 1) {
                    populateCropSeasonSpnr();

                } else {
                    //  utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CropSeasonModel> call, Throwable t) {
                //     utilityofActivity.dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }


    private void populateCropSeasonSpnr() {
        List<String> cropSeasonList = new ArrayList<>();

        for (int i = 0; i < cropSeasonModel.getCropSeasonPayload().size(); i++) {

            cropSeasonList.add(cropSeasonModel.getCropSeasonPayload().get(i).getSeasonName());
        }
        cropSeasonList.add(0, "Select Crop Season");

        //districtList = data.getDistrictPayload().get().getDistrictName();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cropSeasonList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        cropSeasonSpnr.setAdapter(dataAdapter);

    }


    private void crop_name_api() {

//        utilityofActivity.showProgressDialog();

        RetrofitApi.getTickerAppObject().CropNameApi().enqueue(new Callback<CropNameModel>() {
            @Override
            public void onResponse(Call<CropNameModel> call, Response<CropNameModel> response) {
                //              utilityofActivity.dismissProgressDialog();
                cropNameModel = (CropNameModel) response.body();
                if (cropNameModel.getStatus() == 1) {

                    populateCropNameSpnr();

                } else {
                    //  utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CropNameModel> call, Throwable t) {
                //   utilityofActivity.dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }


    private void populateCropNameSpnr() {

        List<String> cropNameList = new ArrayList<>();

        for (int i = 0; i < cropNameModel.getCropNamePayload().size(); i++) {

            cropNameList.add(cropNameModel.getCropNamePayload().get(i).getCropName());
        }

        cropNameList.add(0, "Select Crop Name");

        //districtList = data.getDistrictPayload().get().getDistrictName();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cropNameList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        cropNameSpnr.setAdapter(dataAdapter);

    }


    private void district_api(String state_id) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("state_id", state_id);


        /* DistrictPayload model = new DistrictPayload();
        //utilityofActivity.showProgressDialog();;
        model.setDistrictName("select district name ");
        model.setStateId("xyz");
        model.setId(0);*/
               /* districtModel = (DistrictModel) new DistrictModel();
        districtModel.setDistrictPayload();*/

        RetrofitApi.getTickerAppObject().DistrictApi(params).enqueue(new Callback<DistrictModel>() {
            @Override
            public void onResponse(Call<DistrictModel> call, Response<DistrictModel> response) {
                //      utilityofActivity.dismissProgressDialog();

                districtModel = (DistrictModel) response.body();

           /*     DistrictPayload districtPayload = new DistrictPayload();

                districtPayload.setDistrictName("Enter District Model");
                districtPayload.setId(0);
                districtPayload.setStateId("xyz");

                districtModel.setDistrictPayload(Collections.singletonList(districtPayload));
*/
                if (districtModel.getStatus() == 1) {

                    populateDistrictSpinner();

                } else {
                    //  utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<DistrictModel> call, Throwable t) {
                // utilityofActivity.dismissProgressDialog();
                Log.e("Krishi", "fail");
                t.printStackTrace();
            }
        });
    }


    private void populateDistrictSpinner() {
        List<String> districtList = new ArrayList<>();

        for (int i = 0; i < districtModel.getDistrictPayload().size(); i++) {

            districtList.add(districtModel.getDistrictPayload().get(i).getDistrictName());
        }

        //   districtList.add(0,"Select District Name");
        //districtList = data.getDistrictPayload().get().getDistrictName();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, districtList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        districtSpnr.setAdapter(dataAdapter);

    }


    private void tehsil_api(String district_id) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("district_id", district_id);
        // utilityofActivity.showProgressDialog();

        RetrofitApi.getTickerAppObject().TehsilApi(params).enqueue(new Callback<TehsilModel>() {
            @Override
            public void onResponse(Call<TehsilModel> call, Response<TehsilModel> response) {
                //         utilityofActivity.dismissProgressDialog();
                tehsilModel = (TehsilModel) response.body();
                if (tehsilModel.getStatus() == 1) {

                    populateTehsilSpinner();

                } else {
                    //  utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TehsilModel> call, Throwable t) {
                //   utilityofActivity.dismissProgressDialog();
                //    Log.e("Krishi", "fail");
                t.printStackTrace();
            }
        });
    }


    private void populateTehsilSpinner() {
        List<String> tehsilList = new ArrayList<>();

        for (int i = 0; i < tehsilModel.getTehsilPayload().size(); i++) {

            tehsilList.add(tehsilModel.getTehsilPayload().get(i).getTehsilName());
        }

        tehsilList.add(0, "Select Tehsil Name");

        //districtList = data.getDistrictPayload().get().getDistrictName();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tehsilList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        tehsilSpnr.setAdapter(dataAdapter);

    }

    private void block_api(String tehsil_id) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("tehsil_id", tehsil_id);
        //  utilityofActivity.showProgressDialog();

        RetrofitApi.getTickerAppObject().BlockApi(params).enqueue(new Callback<BlockModel>() {
            @Override
            public void onResponse(Call<BlockModel> call, Response<BlockModel> response) {
                //   utilityofActivity.dismissProgressDialog();
                blockModel = (BlockModel) response.body();
                if (blockModel.getStatus() == 1) {

                    populateBlockSpinner();

                } else {
                    //  utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<BlockModel> call, Throwable t) {
                //        utilityofActivity.dismissProgressDialog();
                //    Log.e("Krishi", "fail");
                t.printStackTrace();
            }
        });
    }


    private void populateBlockSpinner() {

        List<String> blockList = new ArrayList<>();

        for (int i = 0; i < blockModel.getBlockPayload().size(); i++) {

            blockList.add(blockModel.getBlockPayload().get(i).getGirdawarCircleName());
        }

        blockList.add(0, "Select Block Name");
        //districtList = data.getDistrictPayload().get().getDistrictName();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, blockList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        blockSpnr.setAdapter(dataAdapter);

    }

    private void panchayaat_api(String block_id) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("girdawar_id", block_id);
        //  utilityofActivity.showProgressDialog();

        RetrofitApi.getTickerAppObject().PanchayatApi(params).enqueue(new Callback<PanchayatModel>() {
            @Override
            public void onResponse(Call<PanchayatModel> call, Response<PanchayatModel> response) {
                //        utilityofActivity.dismissProgressDialog();
                panchayatModel = (PanchayatModel) response.body();
                if (blockModel.getStatus() == 1) {

                    populatePanchayatSpinner();

                } else {
                    //  utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<PanchayatModel> call, Throwable t) {
                //       utilityofActivity.dismissProgressDialog();
                //    Log.e("Krishi", "fail");
                t.printStackTrace();
            }
        });
    }


    private void populatePanchayatSpinner() {

        List<String> panchayatList = new ArrayList<>();

        for (int i = 0; i < panchayatModel.getPanchayatPayload().size(); i++) {

            panchayatList.add(panchayatModel.getPanchayatPayload().get(i).getPatwarHalkaName());
        }

        panchayatList.add(0, "Select Panchayat Name");
        //districtList = data.getDistrictPayload().get().getDistrictName();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, panchayatList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        panchayatSpnr.setAdapter(dataAdapter);

    }



    private void cceFormSubmitApi() {

        Map<String, String> params = new HashMap<String, String>();
        params.put("authtoken", authToken);
        params.put("crop_season", cropSeasonSpnr.getSelectedItem().toString().trim());
        params.put("insurance_company",insuranceCmpny.getText().toString().trim());
        params.put("crop_name",cropNameSpnr.getSelectedItem().toString().trim());
        params.put("cce_type",cceTypeSpnr.getSelectedItem().toString().trim());
        params.put("country","india");
        params.put("district",districtSpnr.getSelectedItem().toString().trim());
        params.put("tehsil",tehsilSpnr.getSelectedItem().toString().trim());
        params.put("girdawar",blockSpnr.getSelectedItem().toString().trim());
        params.put("patwar_halka","4");
        params.put("gram_panchayat",panchayatSpnr.getSelectedItem().toString().trim());
        params.put("village",villageName.getText().toString().trim());
        params.put("farmer_name",farmerName.getText().toString().trim());
        params.put("random_number",randomNumb.getText().toString().trim());
        params.put("khasra_number",kasraNumb.getText().toString().trim());
        params.put("experiment_id",experimentId.getText().toString().trim());
        params.put("bio_mass_wet_weight","43");
        params.put("wet_weight",wetWeight.getText().toString().trim());
        params.put("date_of_sowing","01-03-2020");
        params.put("irrigated","test");
        params.put("total_plot_area",totalPlotArea.getText().toString().trim());
        params.put("shape_of_cce_plot",shapeOfCceSpnr.getSelectedItem().toString().trim());
        params.put("dimension_of_cce",dimensionCce.getText().toString().trim());
        params.put("moisture_percentage",moisturePrcntg.getText().toString().trim());
        params.put("gov_officer_name",govtOffrName.getText().toString().trim());
        params.put("gov_mobile_number",govtOffrMobNumb.getText().toString().trim());
        params.put("designation",designation.getText().toString().trim());
        params.put("croping_type","abc");
        params.put("cce_status","test");
        params.put("remark",remark.getText().toString().trim());
        params.put("image1",stateSpnr.getSelectedItem().toString().trim());
        params.put("image2",stateSpnr.getSelectedItem().toString().trim());
        params.put("image3",stateSpnr.getSelectedItem().toString().trim());
        params.put("image4",stateSpnr.getSelectedItem().toString().trim());
        params.put("image5",stateSpnr.getSelectedItem().toString().trim());
        params.put("lat1",lat1.getText().toString().trim());
        params.put("long1",long1.getText().toString().trim());
        params.put("lat2",lat2.getText().toString().trim());
        params.put("long2",long2.getText().toString().trim());
        params.put("lat3",lat3.getText().toString().trim());
        params.put("long3",long3.getText().toString().trim());
        params.put("lat4",lat4.getText().toString().trim());
        params.put("long4",long4.getText().toString().trim());
        params.put("lat5",lat4.getText().toString().trim());
        params.put("long5",long4.getText().toString().trim());

        //  utilityofActivity.showProgressDialog();

        RetrofitApi.getTickerAppObject().addCceFormApi(params).enqueue(new Callback<AddCCEFormModel>() {
            @Override
            public void onResponse(Call<AddCCEFormModel> call, Response<AddCCEFormModel> response) {
                //        utilityofActivity.dismissProgressDialog();
                addCCEFormModel = (AddCCEFormModel) response.body();
                if (addCCEFormModel.getStatus() == 1) {



                } else {
                    //  utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddCCEFormModel> call, Throwable t) {
                //       utilityofActivity.dismissProgressDialog();
                //    Log.e("Krishi", "fail");
                t.printStackTrace();
            }
        });
    }




    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_field_distance_btn:

                if(isGps){
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    //     takePictureButton.setEnabled(false);
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                } else {
                    captureImg(REQUEST_IMG_CAPTURE1);
                }}
                else{
                    onGPS();
                }
                break;

            case R.id.img_field_corner_btn:

                if(isGps){
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    // takePictureButton.setEnabled(false);
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                } else {
                    captureImg(REQUEST_IMG_CAPTURE2);
                }}
                else{
                    onGPS();
                }
                break;

            case R.id.img_plot_farmer_btn:

                if (isGps) {

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    // takePictureButton.setEnabled(false);
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                } else {
                    captureImg(REQUEST_IMG_CAPTURE3);
                } } else{
                    onGPS();
                }
                break;

            case R.id.img_surround_field_btn:

                if(isGps){
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    //   takePictureButton.setEnabled(false);
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                } else {
                    captureImg(REQUEST_IMG_CAPTURE4);
                }} else{
                    onGPS();
                }
                break;

            case R.id.btn_change_distance_img:

                imgFieldDistLl.setVisibility(View.VISIBLE);
                sImgFieldDistLl.setVisibility(View.GONE);

                break;

            case R.id.btn_change_corner_img:

                imgFieldCorLl.setVisibility(View.VISIBLE);
                sImgFieldCorLl.setVisibility(View.GONE);

                break;

            case R.id.btn_change_plot_farmer_img:

                imgPlotFarmLl.setVisibility(View.VISIBLE);
                sImgPlotFarmLl.setVisibility(View.GONE);

                break;

            case R.id.btn_change_surround_img:

                imgSurrFieldLl.setVisibility(View.VISIBLE);
                sImgSurrFieldLl.setVisibility(View.GONE);

                break;

            case R.id.layout_btn_save_cce:

                if (validation()) {
                    utilityofActivity.showProgressDialog();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                         //  cceFormSubmitApi();
                            startActivity(getIntent());
                            utilityofActivity.toast(" Successful");
                            utilityofActivity.dismissProgressDialog();
                        }
                    }, 1000);
                }
                break;
        }
    }


    public void captureImg(int reqCode) {
        Intent imageTakeIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (imageTakeIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(imageTakeIntent, reqCode);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMG_CAPTURE1) {
            assert data != null;
            imgFieldDistLl.setVisibility(View.GONE);
            sImgFieldDistLl.setVisibility(View.VISIBLE);
            Bundle extras = data.getExtras();
            Bitmap bitmap = (Bitmap) extras.get("data");
            imgFieldDist_iv.setImageBitmap(bitmap);

            //    Log.e(" Base64 img ", ImageUtils.convert(bitmap));
            getLatLong();

            lat1.setText(latitude);
            long1.setText(longitude);

        }
     /*       LocationManager nManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!nManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                LocationUtils.OnGPS(CCESurvey2Act.this);
            } else {
                LocationUtils.getLocation(CCESurvey2Act.this);

                Log.e(" lat lon val ", LocationUtils.getLocation(CCESurvey2Act.this));

            }*/
            //     setImage(imgFieldDist_iv,data);


        if (requestCode == REQUEST_IMG_CAPTURE2) {
            assert data != null;
            imgFieldCorLl.setVisibility(View.GONE);
            sImgFieldCorLl.setVisibility(View.VISIBLE);
            Bundle extras = data.getExtras();
            Bitmap bitmap = (Bitmap) extras.get("data");
            imgFieldCor_iv.setImageBitmap(bitmap);
            //    setImage(imgFieldCor_iv,data);

            getLatLong();

            lat2.setText(latitude);
            long2.setText(longitude);
        }

        if (requestCode == REQUEST_IMG_CAPTURE3) {
            assert data != null;
            imgPlotFarmLl.setVisibility(View.GONE);
            sImgPlotFarmLl.setVisibility(View.VISIBLE);
            Bundle extras = data.getExtras();
            Bitmap bitmap = (Bitmap) extras.get("data");
            imgPlotFarm_iv.setImageBitmap(bitmap);

            //   setImage(imgPlotFarm_iv,data);

            getLatLong();
            lat3.setText(latitude);
            long3.setText(longitude);

        }
        if (requestCode == REQUEST_IMG_CAPTURE4) {
            assert data != null;
            imgSurrFieldLl.setVisibility(View.GONE);
            sImgSurrFieldLl.setVisibility(View.VISIBLE);
            Bundle extras = data.getExtras();
            Bitmap bitmap = (Bitmap) extras.get("data");
            imgSurrField_iv.setImageBitmap(bitmap);
            //  setImage(imgSurrField_iv,data);

            getLatLong();
            lat4.setText(latitude);
            long4.setText(longitude);
        }
    }


    public void setImage(ImageView image, Intent data) {

        Bundle extras = data.getExtras();
        Bitmap bitmap = (Bitmap) extras.get("data");
        //img_prv.setImageBitmap(bitmap);

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(String.valueOf(bitmap));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert ei != null;
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);

                image.setImageBitmap(rotatedBitmap);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                image.setImageBitmap(rotatedBitmap);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                image.setImageBitmap(rotatedBitmap);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = bitmap;
                image.setImageBitmap(rotatedBitmap);
        }

    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }


    public boolean validation() {

/*        String cropSeason= cropSeasonSpnr.getSelectedItem().toString().trim();
        Log.e("season val ", cropSeason);
        String cropName= cropNameSpnr.getSelectedItem().toString().trim();
        String ceeType= cceTypeSpnr.getSelectedItem().toString().trim();
        String state= stateSpnr.getSelectedItem().toString().trim();
        String district= districtSpnr.getSelectedItem().toString().trim() ;
        Log.e("district val ",district);
        String tehsil= tehsilSpnr.getSelectedItem().toString().trim();
        String block= blockSpnr.getSelectedItem().toString().trim();
        String panchayat= panchayatSpnr.getSelectedItem().toString().trim();
        String farmerType= farmerTypeSpnr.getSelectedItem().toString().trim();
        String cropCond= cropConditionSpnr.getSelectedItem().toString().trim();
        String stress= stressSpnr.getSelectedItem().toString().trim();*/
     /*  String block= blockSpnr.getSelectedItem().toString().trim();
        String block= blockSpnr.getSelectedItem().toString().trim();*/

        if (cropSeasonSpnr.getSelectedItem().toString().trim().length() == 0) {
            utilityofActivity.toast("Please select crop season");
        } else if(insuranceCmpny.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the email id");
        }
        else if (cropNameSpnr.getSelectedItem().toString().trim().length() == 0) {
            utilityofActivity.toast("Please select crop name");
        } else if (cceTypeSpnr.getSelectedItem().toString().trim().length() == 0) {
            utilityofActivity.toast("Please select cce type");
        } else if (stateSpnr.getSelectedItem().toString().trim().length() == 0) {
            utilityofActivity.toast("Please Select State");
        } else if (districtSpnr.getSelectedItem() == null) {
            districtSpnr.setFocusableInTouchMode(true);
//                    ((TextView)districtSpnr.getSelectedView()).setError("Error message");
            utilityofActivity.toast("Please select District");
        } else if (tehsilSpnr.getSelectedItem() == null) {
            utilityofActivity.toast("Please select Tehsil");
        } else if (blockSpnr.getSelectedItem() == null) {
            utilityofActivity.toast("Please select Block");
        } else if (panchayatSpnr.getSelectedItem() == null) {
            utilityofActivity.toast("Please select Panchayat");
        }
        else if(villageName.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the email id");
        }
        else if (farmerTypeSpnr.getSelectedItem() == null) {
            utilityofActivity.toast("Please select Farmer Type");
        } else if(farmerName.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the Farmer Name");
        }
        else if(randomNumb.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the Random number");
        }
        else if(kasraNumb.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the khasra Number");
        }
        else if(experimentId.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the experiment id");
        }else if(wetWeight.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the wet weight");
        }else if(totalPlotArea.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the total plot Area");
        } else if(dimensionCce.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the Dimension of CCE");
        }else if(moisturePrcntg.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the Moisture Percentage");
        } else if(govtOffrName.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the Govt. Officer Name");
        } else if(govtOffrMobNumb.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the Govt. Mobile Number");
        } else if(designation.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the Designation");
        } else if(remark.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the Remark");
        }
        /*else if(cropConditionSpnr.getSelectedItem() == null ) {
            utilityofActivity.toast("Please select crop Condition");
        }*/ else if (stressSpnr.getSelectedItem() == null) {
            utilityofActivity.toast("Please select Stress");
        }
         /*else if(.getSelectedItem().toString().trim().equals("")) {
            utilityofActivity.toast("Please select crop Condition");
        }else if(.getSelectedItem().toString().trim().equals("")) {
            utilityofActivity.toast("Please select crop Condition");
        }*/


        else {
            return true;
        }
        return false;
    }


    public void requestPermission(int request_code) {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, request_code);
    }


    public void getLatLong() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            onGPS();

        } else {
            getLocation();
          //  Log.e(" location value ", getLocation());
        }

    }


    private void onGPS() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                isGps = true;
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                 isGps= false;
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private void getLocation() {
    // check permission again
        if(ActivityCompat.checkSelfPermission(CCESurvey2Act.this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(CCESurvey2Act.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED )
        {
                        requestPermission(REQUEST_CODE1);
        }

        Location locationGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNetwork =  locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        Location locationPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if(locationGps != null){
                latitude = String.valueOf(locationGps.getLatitude());
                longitude = String.valueOf(locationGps.getLongitude());

               // return  lat+" "+longi;
             }
       else if(locationNetwork != null){
            latitude = String.valueOf(locationNetwork.getLatitude());
            longitude = String.valueOf(locationNetwork.getLongitude());

            //return  lat+" "+longi;
        }

        else if(locationPassive != null){
            latitude = String.valueOf(locationPassive.getLatitude());
            longitude = String.valueOf(locationPassive.getLongitude());

        }
        else {

                Toast.makeText(appCompatActivity, " Unable to Get Location", Toast.LENGTH_SHORT).show();
             }
    }

}

