package com.e.krishianand.ui.ccesurvey;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.e.krishianand.R;

public class CameraAc extends AppCompatActivity {

    Button img_btn;
    ImageView img_prv;
    private  static final int REQUEST_IMG_CAPTURE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        img_btn = findViewById(R.id.img_btn);
        img_prv = findViewById(R.id.camera_preview);

        img_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent imageTakeIntent  = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if(imageTakeIntent.resolveActivity(getPackageManager()) != null){
                    startActivityForResult(imageTakeIntent, REQUEST_IMG_CAPTURE);
                }
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_IMG_CAPTURE && requestCode == RESULT_OK){
            Bundle extras = data.getExtras();
            Bitmap bitmap = (Bitmap) extras.get("data");
            img_prv.setImageBitmap(bitmap);
        }}
}