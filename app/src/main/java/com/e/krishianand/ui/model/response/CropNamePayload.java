
package com.e.krishianand.ui.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CropNamePayload {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("crop_name")
    @Expose
    private String cropName;
    @SerializedName("crop_subname")
    @Expose
    private String cropSubname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getCropSubname() {
        return cropSubname;
    }

    public void setCropSubname(String cropSubname) {
        this.cropSubname = cropSubname;
    }

}
