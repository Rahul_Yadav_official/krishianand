package com.e.krishianand.ui.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "cce")
public class CCETable implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private  int id;

    @ColumnInfo(name = "cropseasion")
    private String cropseasion;

    @ColumnInfo(name = "insurance_co")
    private String insuranceco;

    @ColumnInfo(name = "cropname")
    private String cropname;

    @ColumnInfo(name = "ccetype")
    private String ccetype;

@ColumnInfo(name = "country")
    private String country;

    @ColumnInfo(name = "state")
    private String state;

    @ColumnInfo(name = "districtname")
    private String districtname;

    @ColumnInfo(name = "blockname")
    private String blockname;

    @ColumnInfo(name = "hobliname")
    private String hobliname;

    @ColumnInfo(name = "grampanchayat")
    private String grampanchayat;

    @ColumnInfo(name = "villagename")
    private String villagename;

    @ColumnInfo(name = "farmertype")
    private String farmertype;

    @ColumnInfo(name = "farmername")
    private String farmername;

    @ColumnInfo(name = "randomno")
    private String randomno;

    @ColumnInfo(name = "khasrano")
    private String khasrano;

    @ColumnInfo(name = "expid")
    private String expid;

    @ColumnInfo(name = "biomass")
    private String biomass;

    @ColumnInfo(name = "wetweight")
    private String wetweight;

    @ColumnInfo(name = "cropcondition")
    private String cropcondition;

    @ColumnInfo(name = "sowing")
    private String sowing;

    @ColumnInfo(name = "irrigated")
    private String irrigated;

    @ColumnInfo(name = "stress")
    private String stress;

    @ColumnInfo(name = "plotarea")
    private String plotarea;

    @ColumnInfo(name = "shapeplot")
    private String shapeplot;

    @ColumnInfo(name = "dimen_cce")
    private String dimencce;

    @ColumnInfo(name = "moisture")
    private String moisture;

    @ColumnInfo(name = "officername")
    private String officername;

    @ColumnInfo(name = "mobileno")
    private String mobileno;

    @ColumnInfo(name = "designation")
    private String designation;

    @ColumnInfo(name = "croptype")
    private String croptype;

    @ColumnInfo(name = "ccestatus")
    private String ccestatus;

    @ColumnInfo(name = "remark")
    private String remark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCropseasion() {
        return cropseasion;
    }

    public void setCropseasion(String cropseasion) {
        this.cropseasion = cropseasion;
    }

    public String getInsuranceco() {
        return insuranceco;
    }

    public void setInsuranceco(String insuranceco) {
        this.insuranceco = insuranceco;
    }

    public String getCropname() {
        return cropname;
    }

    public void setCropname(String cropname) {
        this.cropname = cropname;
    }

    public String getCcetype() {
        return ccetype;
    }

    public void setCcetype(String ccetype) {
        this.ccetype = ccetype;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrictname() {
        return districtname;
    }

    public void setDistrictname(String districtname) {
        this.districtname = districtname;
    }

    public String getBlockname() {
        return blockname;
    }

    public void setBlockname(String blockname) {
        this.blockname = blockname;
    }

    public String getHobliname() {
        return hobliname;
    }

    public void setHobliname(String hobliname) {
        this.hobliname = hobliname;
    }

    public String getGrampanchayat() {
        return grampanchayat;
    }

    public void setGrampanchayat(String grampanchayat) {
        this.grampanchayat = grampanchayat;
    }

    public String getVillagename() {
        return villagename;
    }

    public void setVillagename(String villagename) {
        this.villagename = villagename;
    }

    public String getFarmertype() {
        return farmertype;
    }

    public void setFarmertype(String farmertype) {
        this.farmertype = farmertype;
    }

    public String getFarmername() {
        return farmername;
    }

    public void setFarmername(String farmername) {
        this.farmername = farmername;
    }

    public String getRandomno() {
        return randomno;
    }

    public void setRandomno(String randomno) {
        this.randomno = randomno;
    }

    public String getKhasrano() {
        return khasrano;
    }

    public void setKhasrano(String khasrano) {
        this.khasrano = khasrano;
    }

    public String getExpid() {
        return expid;
    }

    public void setExpid(String expid) {
        this.expid = expid;
    }

    public String getBiomass() {
        return biomass;
    }

    public void setBiomass(String biomass) {
        this.biomass = biomass;
    }

    public String getWetweight() {
        return wetweight;
    }

    public void setWetweight(String wetweight) {
        this.wetweight = wetweight;
    }

    public String getCropcondition() {
        return cropcondition;
    }

    public void setCropcondition(String cropcondition) {
        this.cropcondition = cropcondition;
    }

    public String getSowing() {
        return sowing;
    }

    public void setSowing(String sowing) {
        this.sowing = sowing;
    }

    public String getIrrigated() {
        return irrigated;
    }

    public void setIrrigated(String irrigated) {
        this.irrigated = irrigated;
    }

    public String getStress() {
        return stress;
    }

    public void setStress(String stress) {
        this.stress = stress;
    }

    public String getPlotarea() {
        return plotarea;
    }

    public void setPlotarea(String plotarea) {
        this.plotarea = plotarea;
    }

    public String getShapeplot() {
        return shapeplot;
    }

    public void setShapeplot(String shapeplot) {
        this.shapeplot = shapeplot;
    }

    public String getDimencce() {
        return dimencce;
    }

    public void setDimencce(String dimencce) {
        this.dimencce = dimencce;
    }

    public String getMoisture() {
        return moisture;
    }

    public void setMoisture(String moisture) {
        this.moisture = moisture;
    }

    public String getOfficername() {
        return officername;
    }

    public void setOfficername(String officername) {
        this.officername = officername;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCroptype() {
        return croptype;
    }

    public void setCroptype(String croptype) {
        this.croptype = croptype;
    }

    public String getCcestatus() {
        return ccestatus;
    }

    public void setCcestatus(String ccestatus) {
        this.ccestatus = ccestatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
