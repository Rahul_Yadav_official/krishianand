package com.e.krishianand.ui.editprofile;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.e.krishianand.MainActivity;
import com.e.krishianand.R;
import com.e.krishianand.api.RetrofitApi;
import com.e.krishianand.ui.login.LoginActivity;
import com.e.krishianand.ui.model.response.ChangePasswordResponse;
import com.e.krishianand.ui.model.response.SignUpResponse;
import com.e.krishianand.ui.registeration.RegisterationActivity;
import com.e.krishianand.util.AppConstant;
import com.e.krishianand.util.PrefrenceFile;
import com.e.krishianand.util.UtilityofActivity;

import java.util.HashMap;
import java.util.Map;

public class ChangePassword extends AppCompatActivity {

    EditText oldPass, newPass, confirmPass;
    Button submit_btn;
    private UtilityofActivity utilityofActivity;
    private AppCompatActivity appCompatActivity = this;
    Context context = this;
    private String authToken;
    ImageView back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        init();
        authToken = PrefrenceFile.getInstance(context).getString("authtoken");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validation()) {
                callResetPassApi();
                }
            }
        });
    }

    private void init() {
        utilityofActivity = new UtilityofActivity(appCompatActivity);
        back = (ImageView)findViewById(R.id.arrowimage);
        oldPass = findViewById(R.id.old_pass_cp);
        newPass = findViewById(R.id.new_pass_cp);
        confirmPass = findViewById(R.id.confm_pass_cp);
        submit_btn = findViewById(R.id.bt_submit_cp);
    }

    public boolean validation() {
        if (oldPass.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the old password");
        } else if (newPass.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the new password");
        } else if (confirmPass.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter confirm password");
        } else if (!newPass.getText().toString().trim().equals(confirmPass.getText().toString().trim())) {
            utilityofActivity.toast("Password confirmation doesn't match");
            confirmPass.setError("The password confirmation does not match");
        } else {
            return true;
        }
        return false;
    }


    private void callResetPassApi() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("authtoken", authToken);
        params.put("oldpassword", oldPass.getText().toString());
        params.put("newpassword", newPass.getText().toString());
        utilityofActivity.showProgressDialog();
        RetrofitApi.getTickerAppObject().changePAsswordApi(params).enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                utilityofActivity.dismissProgressDialog();
                ChangePasswordResponse data = (ChangePasswordResponse) response.body();
                if (data.getStatus() == 1) {

                    utilityofActivity.toast(data.getMessage());
                    Intent open = new Intent(ChangePassword.this, MainActivity.class);
                    startActivity(open);
                    finish();
                }else {
                    utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                utilityofActivity.dismissProgressDialog();
                Log.e("Krishi", "fail");
                t.printStackTrace();
            }
        });
    }
}