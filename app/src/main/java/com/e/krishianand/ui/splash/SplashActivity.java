package com.e.krishianand.ui.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.e.krishianand.MainActivity;
import com.e.krishianand.R;
import com.e.krishianand.ui.login.LoginActivity;
import com.e.krishianand.util.AppConstant;
import com.e.krishianand.util.PrefrenceFile;

public class SplashActivity extends AppCompatActivity {
private Context context=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run()
            {

                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finishAffinity();
                if (PrefrenceFile.getInstance(context).getBoolean(AppConstant.isLogin) == true) {
                    Intent loginIntent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(loginIntent);
                    finish();
                } else {
                    Intent loginIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(loginIntent);
                    finish();
                }
            }
        }, 3000);

    }
}