
package com.e.krishianand.ui.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddCCEFormApiPayload {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("draft_number")
    @Expose
    private String draftNumber;
    @SerializedName("insurance_company")
    @Expose
    private String insuranceCompany;
    @SerializedName("farmer_name")
    @Expose
    private String farmerName;
    @SerializedName("random_number")
    @Expose
    private String randomNumber;
    @SerializedName("crop_season")
    @Expose
    private String cropSeason;
    @SerializedName("district")
    @Expose
    private String district;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDraftNumber() {
        return draftNumber;
    }

    public void setDraftNumber(String draftNumber) {
        this.draftNumber = draftNumber;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getFarmerName() {
        return farmerName;
    }

    public void setFarmerName(String farmerName) {
        this.farmerName = farmerName;
    }

    public String getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(String randomNumber) {
        this.randomNumber = randomNumber;
    }

    public String getCropSeason() {
        return cropSeason;
    }

    public void setCropSeason(String cropSeason) {
        this.cropSeason = cropSeason;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

}
