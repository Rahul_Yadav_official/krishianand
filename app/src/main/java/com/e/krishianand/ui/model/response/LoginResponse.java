package com.e.krishianand.ui.model.response;

public class LoginResponse {
    private int Status;
    private String Message;
    private boolean Error;
    private LoginResponsePayload Payload;
    private String Authtoken;

    public int getStatus() {
        return this.Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return this.Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getError() {
        return this.Error;
    }

    public void setError(boolean Error) {
        this.Error = Error;
    }

    public LoginResponsePayload getPayload() {
        return this.Payload;
    }

    public void setPayload(LoginResponsePayload Payload) {
        this.Payload = Payload;
    }

    public String getAuthtoken() {
        return this.Authtoken;
    }

    public void setAuthtoken(String Authtoken) {
        this.Authtoken = Authtoken;
    }
}
