package com.e.krishianand.ui.login;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.e.krishianand.MainActivity;
import com.e.krishianand.R;
import com.e.krishianand.api.RetrofitApi;
import com.e.krishianand.ui.model.response.LoginResponse;
import com.e.krishianand.ui.registeration.RegisterationActivity;
import com.e.krishianand.util.AppConstant;
import com.e.krishianand.util.PrefrenceFile;
import com.e.krishianand.util.UtilityofActivity;
import com.facebook.FacebookSdk;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private RelativeLayout rlNewUser;
    private AppCompatActivity appCompatActivity = this;
    private Context context = this;
    private TextView forgotText;
    private Button btSignIn;
    private EditText emailEdt, passwordEdt;
    private UtilityofActivity utilityofActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        utilityofActivity = new UtilityofActivity(appCompatActivity);
        rlNewUser = (RelativeLayout) findViewById(R.id.rlNewUser);
        forgotText = (TextView) findViewById(R.id.forgotText);
        emailEdt = (EditText) findViewById(R.id.emailEdt);
        passwordEdt = (EditText) findViewById(R.id.passwordEdt);
        btSignIn = (Button) findViewById(R.id.btSignIn);

        forgotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogForgot();
            }
        });

        rlNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent open = new Intent(LoginActivity.this, RegisterationActivity.class);
                startActivity(open);
            }
        });

        btSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* Intent open = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(open);*/

                if (Validation()) {
                    callLoginApi();
                }
            }
        });
    }

    private void openDialogForgot() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_forget_password);

        Button rlSubmit = (Button) dialog.findViewById(R.id.rlSubmit);
        EditText emailEdt = (EditText) dialog.findViewById(R.id.emailEdt);


        rlSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER | Gravity.RIGHT;
        wmlp.x = -500; //x position
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // dialog.setCancelable(false);
        dialog.show();
    }

    public boolean Validation() {
        if (emailEdt.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the email id");
        } else if (!utilityofActivity.validateEmailAddress(context, emailEdt.getText().toString())) {
            utilityofActivity.toast("Please enter the valid email id");
        } else if (passwordEdt.getText().toString().trim().isEmpty()) {
            utilityofActivity.toast("Please enter the password");
        } else {
            return true;
        }
        return false;
    }

    private void callLoginApi() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", emailEdt.getText().toString());
        params.put("password", passwordEdt.getText().toString());
        utilityofActivity.showProgressDialog();
        RetrofitApi.getTickerAppObject().LoginApi(params).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                utilityofActivity.dismissProgressDialog();
                LoginResponse data = (LoginResponse) response.body();
                if (data.getStatus() == 1) {
                    PrefrenceFile.getInstance(context).setBoolean(AppConstant.isLogin, true);
                    PrefrenceFile.getInstance(context).setString("name", data.getPayload().getName());
                    PrefrenceFile.getInstance(context).setString("mobile", data.getPayload().getContact_num());
                    PrefrenceFile.getInstance(context).setString("email", data.getPayload().getEmail());
                    PrefrenceFile.getInstance(context).setString("imageurl", data.getPayload().getImage_url());
                    PrefrenceFile.getInstance(context).setString("authtoken", data.getAuthtoken());
                    utilityofActivity.toast(data.getMessage());
                    Intent open = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(open);
                    finish();
                } else {
                    utilityofActivity.toast(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                utilityofActivity.dismissProgressDialog();
                Log.e("Krishi", "fail");
                t.printStackTrace();
            }
        });
    }

}